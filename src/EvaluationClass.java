
public class EvaluationClass {
private Double precision;
private Double recall;
private Double fScore;

	public EvaluationClass() {
	precision = (double) 0;
	recall = (double) 0;
	fScore = (double) 0;
	}
	public EvaluationClass(Double precision, Double recall, Double fScore) {
		setPrecision(precision);
		setRecall(recall);
		setFScore(fScore);
		}
	public void setPrecision(Double value)
	{
		precision = value;
	}
	public void setRecall(Double value)
	{
		recall = value;
	}
	public void setFScore(Double value)
	{
		fScore = value;
	}
	
	public Double getPrecision ()
	{
		return this.precision;
	}
	public Double getRecall ()
	{
		return this.recall;
	}
	public Double getFScore ()
	{
		return this.fScore;
	}
}
