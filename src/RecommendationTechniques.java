import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.sql.ConnectionEvent;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;











public class RecommendationTechniques {
	
	
	static final String tableRatingsName="TRAINING_SET";
	static final String USERIDGRAPH = "user_id ";
	static final String ITEMIDGRAPH = "location_id ";
	static final String SESSIONIDGRAPH = "activity_id ";
	static final String SESSION_ID_NAME="ACTIVITY_ID";
	private int user_id = 0;
	Statement statement;
	private int item_id = 0;
	ArrayList<Integer> nearestNeighbors = new ArrayList<Integer>();
	
	
	/////////////////////////	USER_USER COLLABORATIVE FILTERING //////////////////////////////
	
	/*public void User_UserCollaborativeFiltering(String DB_URL,String DB_USERNAME, String DB_PASSWORD,String USER_ID_NAME, String ITEM_ID_NAME, ArrayList<Integer> topNProposalas, int currentUser, Connection connection) throws ClassNotFoundException, SQLException
	{
		
		user_id = currentUser;
		
		Class.forName(JDBC_DRIVER);
	
		
		ResultSet rs = null;
		
		statement = connection.createStatement();
		String query = "";
		
		try {
			nearestNeighbors = findNearestNeighborsJaccardSimilarity(
					connection, statement, USER_ID_NAME, ITEM_ID_NAME);
			// STEP 6 : Check if there is a neighborfood of 1 or 2 and more

			System.out.println("To megethos ths pithanhs geitonias einai "
					+ nearestNeighbors.size());
			if (nearestNeighbors.size() != 0|| nearestNeighbors!=null) {
				if (nearestNeighbors.size() > 1) {
					query = " ( ";
					System.out.println(nearestNeighbors.size());
					for (int counter = 0; counter < nearestNeighbors.size()
							&& counter < NEIGHBORHOODSIZE; counter++) {

						// We get only the first (NeighborfoodSize)

						System.out.println(USER_ID_NAME);

						query = query + "" + USER_ID_NAME + " = "
								+ nearestNeighbors.get(counter) + " OR ";

					}

					query = query.substring(0, query.length() - 3) + " )";
				//	System.out.println(query);
				//	System.out.println("SELECT " + ITEM_ID_NAME
				//			+ ", COUNT(*) as plithos FROM  "+tableRatingsName+"  WHERE "
				//			+ query + " AND RATING > 3 AND " + ITEM_ID_NAME
				//			+ " NOT IN ( SELECT " + ITEM_ID_NAME
				//			+ " FROM  "+tableRatingsName+"  WHERE " + USER_ID_NAME
				//			+ " = " + user_id + " ) GROUP BY "
				//			+ ITEM_ID_NAME + "");
					rs = statement.executeQuery("SELECT " + ITEM_ID_NAME
							+ ", COUNT(*) as plithos FROM  "+tableRatingsName+"  WHERE "
							+ query + " AND RATING > 3 AND " + ITEM_ID_NAME
							+ " NOT IN ( SELECT " + ITEM_ID_NAME
							+ " FROM  "+tableRatingsName+"  WHERE " + USER_ID_NAME
							+ " = " + user_id + " ) GROUP BY "
							+ ITEM_ID_NAME + "");

					while (rs.next()) {
						// STEP 7 : We create an array with all
						// recommendations and their
						// neighborhood count (max = NEIGHBORHOOD SIZE)

						recommendedItemCount.put(
								rs.getInt("" + ITEM_ID_NAME + ""),
								rs.getDouble("plithos"));
					}
				}

				else if (nearestNeighbors.size() == 1)

				// If there is only 1 neighbor
				{

					rs = statement.executeQuery("SELECT " + ITEM_ID_NAME
							+ ", COUNT(*) as plithos FROM  "+tableRatingsName+"  WHERE "
							+ USER_ID_NAME + " = "
							+ nearestNeighbors.get(0) + " AND RATING > 3 AND "
							+ ITEM_ID_NAME + " NOT IN ( SELECT "
							+ ITEM_ID_NAME + " FROM  "+tableRatingsName+"  WHERE "
							+ USER_ID_NAME + " = " + user_id
							+ " ) GROUP BY " + ITEM_ID_NAME + "");
					while (rs.next()) {
					//	System.out.println("Plithos = "
					//			+ rs.getInt("plithos"));
						recommendedItemCount.put(
								rs.getInt("" + ITEM_ID_NAME + ""),
								rs.getDouble("plithos"));
					}
				}
				if(recommendedItemCount.size()>0)
				 {
					 
				 
				// System.out.println(" THa preepei na doume to megethos "+
				// recommendedItemCount.size());
				// STEP 8 : We sort descending the recommendations so as to
				// propose first the
				// top commonest of the neighborhood
				recommendedItemCount = SortHashMap
						.sortByComparator(recommendedItemCount);
				
				for (int key : recommendedItemCount.keySet()) {
				//	System.out.println("To id " + key + " emfanizetai "
					//		+ recommendedItemCount.get(key) + " fores");
				}
				// We check if there are any valid recommendations

				
					
					int count = 0;
					query = "";
					for (int key : recommendedItemCount.keySet()) {
						// if the item currently viewing is on the
						// recommendation list, remove it
						if (key == item_id) {
						} else if (count > NUMOFRECOMMENDATIONS) {
							break;

						} else  {

							
							query = query + " ( " + key + " , 1 ), ";

							topNProposalas.add(key);
							count++;
						}
					}
					query = query.substring(0, query.length() - 2) + " ";
					
					
				}
			}		
			
				// If there are no valid recs, we return none
		}
		catch (Exception e)
		{
			
		}

	
}
	*/
	
	
	
	
	
	
	
	public void User_UserCollaborativeFiltering(ArrayList<Integer> topNProposalas, int currentUser, ArrayList<Record> trainingDataSet, int NEIGHBORHOODSIZE, int NUMOFRECOMMENDATIONS, int MINIMUMRATINGTHRESHOLD, int PICKUSERUSERSIMILARITYMETRIC) throws ClassNotFoundException, SQLException
	{
		
		user_id = currentUser;
		
		
		Map<Integer, Double> recommendedItemCount = new HashMap<Integer, Double>();
		try {
			ArrayList<Integer> itemsPreferred = new ArrayList<Integer>(); 
			if(PICKUSERUSERSIMILARITYMETRIC == 1)
			{
			nearestNeighbors = findNearestNeighborsJaccardSimilarity(
					trainingDataSet, MINIMUMRATINGTHRESHOLD, NEIGHBORHOODSIZE,itemsPreferred);
			// STEP 6 : Check if there is a neighborfood of 1 or 2 and more
			}
			else
			{
				nearestNeighbors = findNearestNeighborsPearsonCorrelation(trainingDataSet, MINIMUMRATINGTHRESHOLD, NEIGHBORHOODSIZE, itemsPreferred);
			}
			System.out.println("To itemspreferred einai : "+itemsPreferred);
			System.out.println("To megethos ths pithanhs geitonias einai "
					+ nearestNeighbors);
			if (nearestNeighbors.size() != 0|| nearestNeighbors!=null) {
				for (int counter = 0; counter < nearestNeighbors.size()
						&& counter < NEIGHBORHOODSIZE; counter++)
				{
					
					System.out.println("To recommened item count einai :"+recommendedItemCount.size());
					int count =0;
					
					for(int i=0;i<trainingDataSet.size();i++)
				{
					
				if(trainingDataSet.get(i).user_id == nearestNeighbors.get(counter))	
				{
					count++;
					if(!itemsPreferred.contains(trainingDataSet.get(i).location_id))
					{
					if(recommendedItemCount.containsKey(trainingDataSet.get(i).location_id))
					{
						recommendedItemCount.put(trainingDataSet.get(i).location_id,recommendedItemCount.get(trainingDataSet.get(i).location_id)+1.00);
					
					}
						else
						{
						
						recommendedItemCount.put(trainingDataSet.get(i).location_id,1.00);
				}
				}
				}
				}
					System.out.println("Mphkame sth loopa "+count+" fores");
				}
				System.out.println("To recommendeditemcount einai : "+recommendedItemCount);
				if(recommendedItemCount.size()>0)
				 {
					 
				 
				// System.out.println(" THa preepei na doume to megethos "+
				// recommendedItemCount.size());
				// STEP 8 : We sort descending the recommendations so as to
				// propose first the
				// top commonest of the neighborhood
				recommendedItemCount = SortHashMap
						.sortByComparator(recommendedItemCount);
				
				for (int key : recommendedItemCount.keySet()) {
					System.out.println("To id " + key + " emfanizetai "
							+ recommendedItemCount.get(key) + " fores");
				}
				// We check if there are any valid recommendations

				
					
					int count = 0;
					
					for (int key : recommendedItemCount.keySet()) {
						// if the item currently viewing is on the
						// recommendation list, remove it
						if (key == item_id) {
						} else if (count >= NUMOFRECOMMENDATIONS) {
							break;

						} else  {

							
						

							topNProposalas.add(key);
							count++;
						}
					}
				
					
					
				}
			}		
			
				// If there are no valid recs, we return none
		}
		catch (Exception e)
		{
			
		}

	
}
/////////////////////////	endof	USER_USER COLLABORATIVE FILTERING //////////////////////////////
	
	
/////////////////////////	ITEM_ITEM COLLABORATIVE FILTERING //////////////////////////////
public void Item_ItemCollaborativeFiltering(ArrayList<Integer> topNProposalas, int currentItem, ArrayList<Record> trainingDataSet, int NUMOFRECOMMENDATIONS, int MINIMUMRATINGTHRESHOLD,int PICKITEMITEMSIMILARITYMETRIC, ArrayList<Integer> friendshipFrom, ArrayList<Integer> friendshipTo, HashMap<Integer, ArrayList<String>> movieGenres, boolean geoSocialRecDataset, String genreMovieSimilarity) throws ClassNotFoundException, SQLException
{
	item_id=currentItem;
	try {
		if(PICKITEMITEMSIMILARITYMETRIC == 1)
		{
		nearestNeighbors = calculateItemCFCosineSimilarity(trainingDataSet, MINIMUMRATINGTHRESHOLD, NUMOFRECOMMENDATIONS);
		}
		else
		{
			nearestNeighbors = findNearestNeighborsSimrankSimilarity(trainingDataSet, NUMOFRECOMMENDATIONS, PICKITEMITEMSIMILARITYMETRIC, friendshipFrom, friendshipTo, movieGenres, geoSocialRecDataset, genreMovieSimilarity);
		}
		// STEP 6 : Check if there is a neighborfood of 1 or 2 and more
		
		if (nearestNeighbors.size() >= 1) {
			
			for (int counter = 0; counter < nearestNeighbors.size()
					&& counter < NUMOFRECOMMENDATIONS; counter++) {

				topNProposalas.add(nearestNeighbors.get(counter));
				
				
			}

		
			
		} 

	
	}catch (Exception e)
	{
		e.printStackTrace();
	}
}
/////////////////////////	endof	ITEM_ITEM COLLABORATIVE FILTERING //////////////////////////////
	
	private ArrayList<Integer> calculateItemCFCosineSimilarity(ArrayList<Record> trainingDataSet, int MINIMUMRATINGTHRESHOLD, int NUMOFRECOMMENDATIONS) {
		ArrayList<Integer> neighborfoodCutomers = new ArrayList<Integer>();
		try {
			TreeMap<Integer, Integer> itemsPearsonsCorrelation = new TreeMap<Integer, Integer>();
			Map<Integer, Double> neighborfoodSimilarity = new HashMap<Integer, Double>();
			/*
			 * Step 1: For pearson Similarity, we want to calculate all ratings
			 * of users, no matter if they are positive or negative
			 */
			for(int i=0;i<trainingDataSet.size();i++)
			{
				if(trainingDataSet.get(i).location_id == item_id)
					itemsPearsonsCorrelation.put(trainingDataSet.get(i).user_id, trainingDataSet.get(i).rating);
			}
		
			if (itemsPearsonsCorrelation.size() < MINIMUMRATINGTHRESHOLD) {
				return neighborfoodCutomers;
			} else {
				/*
				 * Step 2 Checking similar users that have at least one item
				 * preferred as current user and at least one item different
				 * from current user's itemset (So he has a potential
				 * recommendation)
				 */
				ArrayList<Integer> locationsHavingCommonRatingWithCurrentLocation = new ArrayList<Integer>();
				ArrayList<Record> recordsHavingAtLeastOneDifferentPreferenece = new ArrayList<Record>();
				for (int i = 0; i < trainingDataSet.size(); i++)
				{
					if(trainingDataSet.get(i).location_id!=item_id && itemsPearsonsCorrelation.containsKey((trainingDataSet.get(i).user_id)))
						if(!locationsHavingCommonRatingWithCurrentLocation.contains(trainingDataSet.get(i).location_id))
							locationsHavingCommonRatingWithCurrentLocation.add(trainingDataSet.get(i).location_id);
						
				}
				
			//	System.out.println("To locationshavingat least einai : "+locationsHavingCommonRatingWithCurrentLocation);
				Map<Integer, Double> locationsPotentialNearNeighbors = new HashMap<Integer, Double>();
				for (int i = 0; i < trainingDataSet.size(); i++)
				{
					if(locationsHavingCommonRatingWithCurrentLocation.contains(trainingDataSet.get(i).location_id) && !itemsPearsonsCorrelation.containsKey(trainingDataSet.get(i).user_id))
						locationsPotentialNearNeighbors.put(trainingDataSet.get(i).location_id,
								0.00);
				}
				
			//	System.out.println("to potential locations einai : "+locationsPotentialNearNeighbors);

			

		
			TreeMap<Integer, Integer> itemsPreferredByTestingUser = new TreeMap<Integer, Integer>();
				
//				System.out.println(" Plithos omoiwn xrhstwn: "
//						+ locationsPotentialNearNeighbors.size());
//				System.out.println(" Items pearson Correlation: "
//						+ itemsPearsonsCorrelation);
				for(int key : locationsPotentialNearNeighbors.keySet())
				{
				for(int i=0;i<trainingDataSet.size();i++)
				{
					if(trainingDataSet.get(i).location_id == key && itemsPearsonsCorrelation.containsKey(trainingDataSet.get(i).user_id))
					{
						itemsPreferredByTestingUser.put(trainingDataSet.get(i).user_id, trainingDataSet.get(i).rating);
					}
				}
			double[] v1 = new double[itemsPreferredByTestingUser.size()];// We store here the
				// ratings of
				// current user for
				// corrated items

				double[] v2 = new double[itemsPreferredByTestingUser.size()];// we store here the
				int counter =0;
				for(int itemsCurrentUser :itemsPreferredByTestingUser.keySet())
				{
					if(itemsPearsonsCorrelation.containsKey(itemsCurrentUser))
					{
						v1[counter]=itemsPearsonsCorrelation.get(itemsCurrentUser);
						v2[counter]=itemsPreferredByTestingUser.get(itemsCurrentUser);
					}
					counter++;
				}
				if(v1.length>1)
				{
				Double similarity = calculateCosineSimilarity(v1, v2);
				// We check if there is a positive similarity between users
										if (!(similarity.isNaN() || similarity<0))
										{
											neighborfoodSimilarity.put(key,
													similarity);
										}
									}
								}
								
			}
			//System.out.println("To neighborhood similarity einai :"+neighborfoodSimilarity);
				if (neighborfoodSimilarity.size() != 0) {
					

					neighborfoodSimilarity = SortHashMap
							.sortByComparator(neighborfoodSimilarity);
					
					//Here we propose the top - (NEIGHBORHOODSIZE) users
					int i = 0;
					for (Iterator<Entry<Integer, Double>> it = neighborfoodSimilarity
							.entrySet().iterator(); it.hasNext()
							&& i < NUMOFRECOMMENDATIONS;) {
						Entry<Integer, Double> neighbor = it.next();

						neighborfoodCutomers.add(neighbor.getKey());
						i++;
					}
					return neighborfoodCutomers;
				}
				else
				{
					return neighborfoodCutomers;
				}
				
		
	
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return neighborfoodCutomers;
	
	
}
	private ArrayList<Integer> findNearestNeighborsPearsonCorrelation(
			ArrayList<Record> trainingDataSet, int MINIMUMRATINGTHRESHOLD, int NEIGHBORHOODSIZE, ArrayList<Integer> itemsPreferred) {
		ArrayList<Integer> neighborfoodCutomers = new ArrayList<Integer>();
		try {
			TreeMap<Integer, Double> itemsPearsonsCorrelation = new TreeMap<Integer, Double>();
			Map<Integer, Double> neighborfoodSimilarity = new HashMap<Integer, Double>();
			/*
			 * Step 1: For pearson Similarity, we want to calculate all ratings
			 * of users, no matter if they are positive or negative
			 */
			for(int i=0;i<trainingDataSet.size();i++)
			{
				if(trainingDataSet.get(i).user_id == user_id)
					itemsPearsonsCorrelation.put(trainingDataSet.get(i).location_id, (double) trainingDataSet.get(i).rating);
			}
		
			if (itemsPearsonsCorrelation.size() < MINIMUMRATINGTHRESHOLD) {
				return neighborfoodCutomers;
			} else {
				/*
				 * Step 2 Checking similar users that have at least one item
				 * preferred as current user and at least one item different
				 * from current user's itemset (So he has a potential
				 * recommendation)
				 */
				ArrayList<Integer> usersHavingAtLeastOneSamePreferenece = new ArrayList<Integer>();
				ArrayList<Record> recordsHavingAtLeastOneDifferentPreferenece = new ArrayList<Record>();
				for (int i = 0; i < trainingDataSet.size(); i++)
				{
					if(trainingDataSet.get(i).user_id!=user_id && itemsPearsonsCorrelation.containsKey((trainingDataSet.get(i).location_id)))
						if(!usersHavingAtLeastOneSamePreferenece.contains(trainingDataSet.get(i).user_id))
							usersHavingAtLeastOneSamePreferenece.add(trainingDataSet.get(i).user_id);
						
				}
				
				System.out.println("To usersHavingnat least einai : "+usersHavingAtLeastOneSamePreferenece);
				Map<Integer, Double> personSimilarity = new HashMap<Integer, Double>();
				for (int i = 0; i < trainingDataSet.size(); i++)
				{
					if(usersHavingAtLeastOneSamePreferenece.contains(trainingDataSet.get(i).user_id) && !itemsPearsonsCorrelation.containsKey(trainingDataSet.get(i).location_id))
						personSimilarity.put(trainingDataSet.get(i).user_id,
								0.00);
				}
				
				System.out.println("to personSimilarity einai : "+personSimilarity);

			

		
			Map<Integer, Integer> itemsPreferredByTestingUser = new HashMap<Integer, Integer>();
				
				System.out.println(" Plithos omoiwn xrhstwn: "
						+ personSimilarity.size());
				for(int key : personSimilarity.keySet())
				{
				for(int i=0;i<trainingDataSet.size();i++)
				{
					if(trainingDataSet.get(i).user_id == key && itemsPearsonsCorrelation.containsKey(trainingDataSet.get(i).location_id))
					{
						itemsPreferredByTestingUser.put(trainingDataSet.get(i).location_id, trainingDataSet.get(i).rating);
					}
				}
				double[] v1 = new double[itemsPreferredByTestingUser.size()];// We store here the
				// ratings of
				// current user for
				// corrated items

				double[] v2 = new double[itemsPreferredByTestingUser.size()];// we store here the
				int counter =0;
				for(int itemsCurrentUser :itemsPearsonsCorrelation.keySet())
					if(itemsPreferredByTestingUser.containsKey(itemsCurrentUser))
					{
						v1[counter]=itemsPearsonsCorrelation.get(itemsCurrentUser);
						v2[counter]=itemsPreferredByTestingUser.get(itemsCurrentUser);
					}
				
				if(v1.length>1)
				{
				Double similarity = calculatePearsonSimilarity(v1, v2);
				// We check if there is a positive similarity between users
										if (!(similarity.isNaN() || similarity<0))
										{
											neighborfoodSimilarity.put(key,
													similarity);
										}
									}
								}
								
			}
			
				if (neighborfoodSimilarity.size() != 0) {
					
					for (int key : neighborfoodSimilarity.keySet()) {
						System.out.println("H Omoiothta me to xrhsth " + key
								+ " einai " + neighborfoodSimilarity.get(key));
					}

					neighborfoodSimilarity = SortHashMap
							.sortByComparator(neighborfoodSimilarity);
					System.out.println(neighborfoodSimilarity);
					//Here we propose the top - (NEIGHBORHOODSIZE) users
					int i = 0;
					for (Iterator<Entry<Integer, Double>> it = neighborfoodSimilarity
							.entrySet().iterator(); it.hasNext()
							&& i < NEIGHBORHOODSIZE;) {
						Entry<Integer, Double> neighbor = it.next();

						neighborfoodCutomers.add(neighbor.getKey());
						i++;
					}
					return neighborfoodCutomers;
				}
				else
				{
					return neighborfoodCutomers;
				}
				
		
	
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return neighborfoodCutomers;
	}
private ArrayList<Integer> findNearestNeighborsJaccardSimilarity(ArrayList<Record> trainingDataSet, int MINIMUMRATINGTHRESHOLD, int NEIGHBORHOODSIZE, ArrayList<Integer> itemsPreferred)
{
	ArrayList<Integer> nearestNeighbors = new ArrayList<Integer>();
	try {
		 
		Map<Integer, Double> personSimilarity = new HashMap<Integer, Double>();
		
				
		/*
		 * Step 1: Calculating current user's objects that has rated
		 * positevely >3
		 */
		
	for(int i=0;i<trainingDataSet.size();i++)
		/*
		 * Creating an array for holding items preferences
		 */
		{
		if(trainingDataSet.get(i).user_id == user_id && trainingDataSet.get(i).rating>3)
		{
			if (!itemsPreferred.contains(trainingDataSet.get(i).location_id))
			itemsPreferred.add(trainingDataSet.get(i).location_id);
		}
		
		}
	System.out.println("to items preferred gia ton xrhsth "+user_id+" einai : "+itemsPreferred);
		/*
		 * If the current user has no preferred items, return none, else
		 * continue to Step 2
		 */

		if (itemsPreferred.size() >= MINIMUMRATINGTHRESHOLD) {
			/*
			 * Step 2 Checking similar users that have at least one item
			 * preferred as current user and at least one item different
			 * from current user's itemset (So he has a potential
			 * recommendation)
			 */
			ArrayList<Integer> usersHavingAtLeastOneSamePreferenece = new ArrayList<Integer>();
			ArrayList<Record> recordsHavingAtLeastOneDifferentPreferenece = new ArrayList<Record>();
			for (int i = 0; i < trainingDataSet.size(); i++)
			{
				if(trainingDataSet.get(i).user_id!=user_id && itemsPreferred.contains(trainingDataSet.get(i).location_id))
					if(!usersHavingAtLeastOneSamePreferenece.contains(trainingDataSet.get(i).user_id))
						usersHavingAtLeastOneSamePreferenece.add(trainingDataSet.get(i).user_id);
					
			}
			
			System.out.println("To usersHavingnat least einai : "+usersHavingAtLeastOneSamePreferenece);
			for (int i = 0; i < trainingDataSet.size(); i++)
			{
				if(usersHavingAtLeastOneSamePreferenece.contains(trainingDataSet.get(i).user_id) && !itemsPreferred.contains(trainingDataSet.get(i).location_id))
					personSimilarity.put(trainingDataSet.get(i).user_id,
							0.00);
			}
			
			System.out.println("to personSimilarity einai : "+personSimilarity);

		

		
			
			System.out.println(" Plithos omoiwn xrhstwn: "
					+ personSimilarity.size());

			jaccardSimilarity(personSimilarity,itemsPreferred, trainingDataSet);
			// STEP 5 :Remove the persons who dont offer to the neighborhood

			int counter = 0;
			for (Integer key : personSimilarity.keySet()) {
				if (counter >= NEIGHBORHOODSIZE) {
					break;
				} else {
					nearestNeighbors.add(key);
					counter++;
				}

			}
			return nearestNeighbors;

			// If there are no valid recs, we return none

		} else {
			return nearestNeighbors;
		}

	} catch (Exception e) {
		e.printStackTrace();
		
	}
	return nearestNeighbors;
}

	private ArrayList<Integer> findNearestNeighborsSimrankSimilarity (ArrayList<Record> trainingDataSet, int NUMOFRECOMMENDATIONS, int PICKITEMITEMSIMILARITYMETRIC, ArrayList<Integer> friendshipFrom, ArrayList<Integer> friendshipTo, HashMap<Integer, ArrayList<String>> movieGenres, boolean geoSocialRecDataset, String genreMovieSimilarity)
	{
		ArrayList<Integer> neighborfoodCutomers = new ArrayList<Integer>();
		try {
			
			Map<Integer, Double> neighborfoodSimilarity = new HashMap<Integer, Double>();
			ResultSet rs;
			
//		/*	rs = statement.executeQuery("SELECT DISTINCT " + USER_ID_NAME
//					+ ", rating from  "+tableRatingsName+"  where " + ITEM_ID_NAME + " = "
//					+ item_id + " AND rating > 3 GROUP BY "
//					+ USER_ID_NAME);
//				System.out.println("SELECT DISTINCT " + USER_ID_NAME
//						+ ", rating from  "+tableRatingsName+"  where " + ITEM_ID_NAME + " = "
//						+ item_id + " AND rating > 3 GROUP BY "
//						+ USER_ID_NAME);
//			while (rs.next())
//			/*
//			 * Creating an array for holding items preferences
//			 */
//			{
//				itemsPearsonsCorrelation.put(rs.getInt(USER_ID_NAME),
//						(double) rs.getInt("rating"));
//				// System.out.println (" O xrhsths protimaei to "
//				// +rs.getInt(ITEM_ID) + " me rating : " + (double)
//				// rs.getInt("rating"));
//			}
//			if (itemsPearsonsCorrelation.size() < MINIMUMRATINGTHRESHOLD) {
//				return neighborfoodCutomers;
//			} else {
//
//				String query = " AND ( ";
//				for (Integer key : itemsPearsonsCorrelation.keySet()) {
//					query = query + "  "+tableRatingsName+" ." + USER_ID_NAME + " = " + key
//							+ " OR";
//
//				}
//				query = query.substring(0, query.length() - 3) + ")";
//				System.out
//						.println("SELECT DISTINCT * FROM  "+tableRatingsName+"  WHERE rating > 3 "
//								+ query);
//
//				rs = statement
//						.executeQuery("SELECT DISTINCT * FROM  "+tableRatingsName+"  WHERE rating > 3 "
//								+ query);
//
//				WebGraph wg = new WebGraph();
//				while (rs.next()) {
//					wg.addLink("user_id " + rs.getInt(USER_ID_NAME), "item_id "
//							+ rs.getInt(ITEM_ID_NAME), 1.00);
//						
//					// query = "AND ( ";
//					// for (Integer key : itemsPearsonsCorrelation.keySet())
//					// {
//					// query = query + ITEM_ID + " = " + key + " OR ";
//					// tmTest.put(key, 0.00);
//					// }
//				}
//				if(wg.numNodes()>0)
//				{
//				double[][] matrixData = new double[wg.numNodes()][wg.numNodes()];
//				for (int i = 1; i <= wg.numNodes(); i++) {
//
//					for (Object outLinksLink1 : wg.outLinks(i).keySet()) {
//						int y = (Integer) outLinksLink1;
//						matrixData[i - 1][y - 1] = 1.00 / wg.inLinks(y).size();
//					}
//				}
//				// We define the item_id which we need to calculate the
//				// similarity measures
//				int itemIDIdentifier = 0;
//
//				for (int i = 1; i <= wg.numNodes(); i++) {
//					if (wg.IdentifyerToURL(i).equals("item_id " + item_id))
//
//						itemIDIdentifier = i;
//				}
	///////////////We try something else //////////////////////////
			WebGraph wg = new WebGraph();
			WebGraph wgWithParallelLinks = new WebGraph();
			for(int i=0;i<trainingDataSet.size();i++)
				if(trainingDataSet.get(i).rating>3)
				{
					wg.addLink(USERIDGRAPH+""+trainingDataSet.get(i).user_id, ITEMIDGRAPH+""+trainingDataSet.get(i).location_id,1.00);
					wgWithParallelLinks.addLink(USERIDGRAPH+""+trainingDataSet.get(i).user_id, ITEMIDGRAPH+""+trainingDataSet.get(i).location_id,1.00);
					if(geoSocialRecDataset)
					{
					wg.addLink(ITEMIDGRAPH+""+trainingDataSet.get(i).location_id, SESSIONIDGRAPH+""+trainingDataSet.get(i).activity_id ,1.00);
					
					wgWithParallelLinks.addLink(ITEMIDGRAPH+""+trainingDataSet.get(i).location_id, SESSIONIDGRAPH+""+trainingDataSet.get(i).activity_id ,1.00);
					}
					}
			if(geoSocialRecDataset)
			{
			for (int i=0;i<friendshipFrom.size();i++)
			{
				wg.addLink(USERIDGRAPH+""+friendshipFrom.get(i), USERIDGRAPH+""+friendshipTo.get(i), 1.00);
				wgWithParallelLinks.addLink(USERIDGRAPH+""+friendshipFrom.get(i), USERIDGRAPH+""+friendshipTo.get(i), 1.00);
			}
			} 
			else
			{
				//We have to check for non existing nodes of location
				
				for(int key : movieGenres.keySet())
				{
					if(wg.inLinks(key).size()>0)
					{
					ArrayList<String> genreForEachMovie = movieGenres.get(key);
					for(int i=0;i<genreForEachMovie.size();i++)
					{
						
						wg.addLink(ITEMIDGRAPH+""+key, SESSIONIDGRAPH+""+genreForEachMovie.get(i),1.00);
						wgWithParallelLinks.addLink(ITEMIDGRAPH+""+key, SESSIONIDGRAPH+""+genreForEachMovie.get(i),1.00);
					}
				}
					
				}
				BufferedReader br = null;
				String line = "";
				String cvsSplitBy = ",";
				br = new BufferedReader(new FileReader(genreMovieSimilarity));
				
				while ((line = br.readLine()) != null) {
					
				        // use comma as separator
					String[] USER_ID = line.split(cvsSplitBy);
					if(Double.valueOf(USER_ID[2])> 0.98)
					{
						wg.addLink(SESSIONIDGRAPH+""+USER_ID[0], SESSIONIDGRAPH+""+USER_ID[1],1.00);
						wg.addLink(SESSIONIDGRAPH+""+USER_ID[1], SESSIONIDGRAPH+""+USER_ID[0],1.00);
						wgWithParallelLinks.addLink(SESSIONIDGRAPH+""+USER_ID[1], SESSIONIDGRAPH+""+USER_ID[0],1.00);
						wgWithParallelLinks.addLink(SESSIONIDGRAPH+""+USER_ID[0], SESSIONIDGRAPH+""+USER_ID[1],1.00);
					}
						
				}
			}
			System.out.println("O graphos einai wg :"+wg.numNodes());
				SimRank sm =new SimRank(wg, wgWithParallelLinks,geoSocialRecDataset);
				System.out.println(neighborfoodSimilarity);
				sm.simrankScores(ITEMIDGRAPH, item_id,neighborfoodSimilarity, PICKITEMITEMSIMILARITYMETRIC);
				neighborfoodSimilarity = SortHashMap.sortByComparator(neighborfoodSimilarity);
				//calculateSimrankMatrices(matrixData, wg, itemIDIdentifier,
					//	neighborfoodSimilarity);
		
			


			for (int key : neighborfoodSimilarity.keySet()) {
				System.out.println("H Omoiothta tou xrhsth "+item_id+" me to xrhsth " + key
						+ " einai " + neighborfoodSimilarity.get(key));
			}
			int i = 0;
			for (Iterator<Entry<Integer, Double>> it = neighborfoodSimilarity
					.entrySet().iterator(); it.hasNext()
					&& i < NUMOFRECOMMENDATIONS;) {
				Entry<Integer, Double> neighbor = it.next();

				neighborfoodCutomers.add(neighbor.getKey());
				i++;
			}
			for (int x = 0; x < neighborfoodCutomers.size(); x++)
				System.out.println("Sth geitonia yparxoun oi :"
						+ neighborfoodCutomers.get(x));
			
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return neighborfoodCutomers;
	}
	/////////////////////   METHODS OF CALCULATING SIMILARITY /////////////////////////
	private double calculatePearsonSimilarity(double[] v1, double[] v2) {
		PearsonsCorrelation pc = new PearsonsCorrelation();
		double v3 = pc.correlation(v1, v2);
		return v3;

	}
	private double calculateCosineSimilarity(double[] v1, double[] v2) {
		double dotProduct = 0.0;
		double normA = 0.0;
		double normB = 0.0;
	//	System.out.println("To v1 eiani :"+v1);
	//	System.out.println("To v2 eiani :"+v2);
		for (int i = 0; i < v1.length; i++) {
	//		System.out.println("To v1 einai "+ v1[i]+ " enw yo v2 einai : "+v2[i]);
			dotProduct += v1[i] * v2[i];
			// System.out.println (dotProduct);
			normA += Math.pow(v1[i], 2);
			normB += Math.pow(v2[i], 2);
		}
		return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}
	
	public void jaccardSimilarity(Map<Integer, Double> personSimilarity, ArrayList<Integer> itemsPreferredByCurrentUser, 
			 ArrayList<Record> trainingDataSet)  {
		// STEP 3 : Similarity Calculation
		
		
		
		for (Integer key : personSimilarity.keySet()) {
			ArrayList<Integer> preferencesofTestingUser = new ArrayList<Integer>();
			for(int i=0;i<trainingDataSet.size();i++)
			{
				if(trainingDataSet.get(i).user_id==key)
					if(!preferencesofTestingUser.contains(trainingDataSet.get(i).location_id))
					preferencesofTestingUser.add(trainingDataSet.get(i).location_id);	
			}
			System.out.println("To itemspreferredbycurrentuser einai :"+itemsPreferredByCurrentUser);
			System.out.println("To preferencesoftestunguser einai :"+preferencesofTestingUser);
			Double samePreferences = 0.00;
			Double differentPreferences = 0.00;
			for(int i=0;i<preferencesofTestingUser.size();i++)
				if(itemsPreferredByCurrentUser.contains(preferencesofTestingUser.get(i)))
					samePreferences++;
				else 
					differentPreferences++;
		
			System.out.println("Idies protimhseis " + samePreferences
					+ " me ton xrhsth " + key);

			// Getting all pair's total preferences, so as to count next
			// SAME/TOTAL -> Jaccard

			
			double similarity = samePreferences / (samePreferences+differentPreferences) ;
			System.out.println("Omoiothta me to xrhsth " + key + " : "
					+ similarity);
			personSimilarity.put(key, similarity);

		}
	

		// STEP 4 : Sort to a descending order so as to be able to get the most
		// similar
		personSimilarity = SortHashMap.sortByComparator(personSimilarity);
		for (Integer key : personSimilarity.keySet()) {

			System.out.println("Pinakas omoiothtwn \n" + key
					+ " me omoiothta : " + personSimilarity.get(key));
		}
	}
	public static void calculateSimrankMatrices(double[][] matrixData,
			WebGraph wg, int itemIdentifier,
			Map<Integer, Double> neighborfoodSimilarity) {

		RealMatrix I = MatrixUtils.createRealIdentityMatrix(wg.numNodes()
				* wg.numNodes());

		RealMatrix I2 = MatrixUtils.createRealIdentityMatrix(wg.numNodes());
		RealMatrix W;
		// We create the W matrix which is the transition probability matrix

		W = MatrixUtils.createRealMatrix(matrixData);
		// System.out.println(W.getColumnDimension());

		// We calculate the kronecker product W' x W'
		KroneckerProduct kp = new KroneckerProduct(W.transpose(), W.transpose());
		// Calculate inverseOf[I-c(W' x W')]
		RealMatrix pinverse = new LUDecomposition(I.subtract(kp
				.calculateProduct().scalarMultiply(0.8))).getSolver()
				.getInverse();
		RealMatrix S = pinverse.scalarMultiply(0.2).multiply(
				kp.vectorization(I2));
		// System.out.println(S);
		S = kp.deVectorization(pinverse.scalarMultiply(0.2).multiply(
				kp.vectorization(I2)));
		System.out.println("To identifier einai " + (itemIdentifier - 1));
		int URLToIdentifier = 0;

		for (int i = 0; i < S.getRow(itemIdentifier - 1).length; i++)

		{
			URLToIdentifier = Integer.valueOf(wg.IdentifyerToURL(i + 1)
					.substring(8));
			neighborfoodSimilarity.put(URLToIdentifier,
					S.getRow(itemIdentifier - 1)[i]);

		}

		System.out.println(S);

	}
}