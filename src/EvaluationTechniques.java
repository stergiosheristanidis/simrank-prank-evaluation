import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EvaluationTechniques {

	public EvaluationClass countPrecisionUserBasedCF(ArrayList<Integer> topNProposalas, int userID, ArrayList<Record> testDataSet) throws ClassNotFoundException, SQLException
	{
		EvaluationClass ec= new EvaluationClass();
		
		Double precision = 0.0;
		Double recall = 0.0;
		Double fScore = 0.0;
		
		ArrayList<Integer> testSetItems = new ArrayList<Integer>();
		for(int i=0;i<testDataSet.size();i++)
		{
			if(testDataSet.get(i).user_id == userID)
				testSetItems.add(testDataSet.get(i).location_id);
		}
		if (testSetItems.size()>=2)
		{
		for(int i=0;i<topNProposalas.size();i++)
		{
			if(testSetItems.contains(topNProposalas.get(i)))
			{
				precision+=1;
				recall+=1;
			}	
		}
		if(!precision.equals(0.0))
		precision = precision/topNProposalas.size();
		if(!recall.equals(0.0))
			recall = recall/testSetItems.size();
		if(recall.equals(0.0) && precision.equals(0.0))
		{
			fScore=0.0;
		}
		else
		{
		fScore= (2*precision*recall)/(precision+recall);
		}
		}
		else
		{
			precision = null;
			recall = null;
			fScore=null;
		}
		ec.setFScore(fScore);
		ec.setPrecision(precision);
		ec.setRecall(recall);
		
		return ec;
	}
	public EvaluationClass countPrecisionItemBasedCF(ArrayList<Integer> topNProposalas, int itemID,ArrayList<Record> trainingDataSet, ArrayList<Record> testDataSet) 
	{
		EvaluationClass ec= new EvaluationClass();
		
		Double sumPrecision = 0.0;
		Double sumRecall = 0.0;
		Double sumFScore = 0.0;
	
	int countTotalScores = 0;
	
		ArrayList<Integer> testSetItems = new ArrayList<Integer>();
		ArrayList<Integer> testSetUsers = new ArrayList<Integer>();
		ArrayList<EvaluationClass> testSetTotals = new ArrayList<EvaluationClass>();
	for(int i=0;i<trainingDataSet.size();i++)
	{
		if(trainingDataSet.get(i).location_id == itemID && trainingDataSet.get(i).rating>3)
			testSetUsers.add(trainingDataSet.get(i).user_id);
	}
		
		if (testSetUsers.size()!=0)
		{
			for (int y=0;y<testSetUsers.size();y++)
			{
				Double precision = 0.0;
				Double recall = 0.0;
				Double fScore = 0.0;
				for(int i=0;i<testDataSet.size();i++)
				{
					if(testDataSet.get(i).user_id==testSetUsers.get(y) && testDataSet.get(i).rating>3)
						testSetItems.add(testDataSet.get(i).location_id);
				}
			
		if (testSetItems.size()>2)
		{
		for(int i=0;i<topNProposalas.size();i++)
		{
			if(testSetItems.contains(topNProposalas.get(i)))
			{
				precision++;
				recall++;
			}	
		}
		if(!precision.equals(0.0))
		{
			countTotalScores++;
		precision = precision/topNProposalas.size();
		sumPrecision+=precision;
		}
		else
		{
			countTotalScores++;
			precision = 0.00;
		}
		if(!recall.equals(0.0))
		{
			recall = recall/testSetItems.size();
			sumRecall+=recall;
		}
			if(recall.equals(0.0) && precision.equals(0.0))
		{
			fScore=0.0;
		}
		else
		{
		fScore= (2*precision*recall)/(precision+recall);
		sumFScore+=fScore;
		}
		}
		
		
			}
		}
			if(countTotalScores == 0)
			{
				ec.setFScore(null);
				ec.setPrecision(null);
				ec.setRecall(null);
			}
			else
			{
				ec.setFScore(sumFScore/countTotalScores);
				ec.setPrecision(sumPrecision/countTotalScores);
				ec.setRecall(sumRecall/countTotalScores);
			}
			return ec;
		
	
	}
}
