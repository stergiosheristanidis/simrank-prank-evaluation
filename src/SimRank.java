
import java.util.*;

import javax.swing.text.html.InlineView;

/**
 *  SimRank is an iterative PageRank-like method for computing similarity.
 *  It goes beyond direct cocitation for computing similarity much as PageRank
 *  goes beyond direct linking for computing importance.
 * 
 * @author Bruno Martins
 */
public class SimRank {
	static final double dampFactor = 0.80;
	static final double lamda = 0.50;
	static final double alpha = 0.25;
	static final double beta = 0.25;
	static final double gama = 0.25;
	static final double delta = 0.25;
	
	WebGraph graph;
	WebGraph graphWithParallelLinks;
	Map<Integer, HashMap<Integer, Double>> pRankScores = new HashMap<Integer, HashMap<Integer,Double>>();
	Map<Integer, Map> parallelInLinks = new HashMap<Integer, Map>();
	Map<Integer, Map> parallelOutLinks = new HashMap<Integer, Map>();
	boolean chechForParallelLinks = false;
	public SimRank(WebGraph graph, WebGraph wgWithParallelLinks, boolean geoSocialRecDataset)
	{
		this.graph = graph;
		this.graphWithParallelLinks = wgWithParallelLinks;
		chechForParallelLinks = geoSocialRecDataset;
	}
	
public void simrankScores(String domain, int domainNodeID, Map<Integer, Double> similarityPerNeighbor, int PICKITEMITEMSIMILARITYMETRIC)
{
Map<Integer, HashMap<Integer, Double>> similarityScores = new HashMap<Integer, HashMap<Integer,Double>>();
Map<Integer, HashMap<Integer, Double>> oldSimilarityScores = new HashMap<Integer, HashMap<Integer,Double>>();
Map<Integer, Double> neighborfoodSimilarity = new HashMap<Integer, Double>();
int counter = 1;
	
	boolean converge = false;
	//if(chechForParallelLinks)
	//discoverParallelLinks(graph, graphWithParallelLinks, parallelInLinks, parallelOutLinks);
	
while(!converge)
{

	for (int i =1; i<= graph.numNodes();i++)
	{
		HashMap <Integer, Double> scores = new HashMap<Integer, Double>();
		
		for (int y=1; y<i;y++)
		{
			//We calculate similarity scores only for nodes of the same domain and only between them and the one we are currently viewing
		//System.out.println("vrhkame diana "+domain+""+domainNodeID);
			if(graph.IdentifyerToURL(y).contains(domain) &&graph.IdentifyerToURL(i).contains(domain) && ((graph.IdentifyerToURL(i).equals(domain+""+domainNodeID))||(graph.IdentifyerToURL(y).equals(domain+""+domainNodeID)))) 
			{
				//System.out.println ("twra exetazoume tous komvous "+i+" kai "+y);
				if(PICKITEMITEMSIMILARITYMETRIC == 2)
				{
			scores.put(y, calculateSimrank(i, y, counter));
				}
				else if (PICKITEMITEMSIMILARITYMETRIC == 3)
				{
					scores.put(y, calculatePrank(i, y, counter));	
				}
				else
				{
					scores.put(y, calculateOmnirank(i, y, counter));
				}
			}
		}
	similarityScores.put(i, scores);		
	}
	
	for ( int key: similarityScores.keySet())
		for ( int keyInner: similarityScores.get(key).keySet())
		{
		//	System.out.println("H omoiothta toy komvou "+graph.IdentifyerToURL(key)+" me ton komvo "+graph.IdentifyerToURL(keyInner) +" einai : "+similarityScores.get(key).get(keyInner)+"\n");
			neighborfoodSimilarity.put(Integer.valueOf(graph.IdentifyerToURL(key).substring(domain.length())), similarityScores.get(key).get(keyInner));
			neighborfoodSimilarity.put(Integer.valueOf(graph.IdentifyerToURL(keyInner).substring(domain.length())), similarityScores.get(key).get(keyInner));
			
		}	
	if(oldSimilarityScores.equals(similarityScores) || counter == 4)
	{
		converge = true;
	}
	else
	{
		oldSimilarityScores.putAll(similarityScores);
		counter ++;
	}
	neighborfoodSimilarity =  SortHashMap.sortByComparator(neighborfoodSimilarity);
	System.out.println(neighborfoodSimilarity);
	System.out.println("O arithmos twn iterations einai "+counter);
	System.out.println("O arithmos ypopsifiwn geitonwn einai  "+neighborfoodSimilarity.size());
	similarityPerNeighbor.putAll(neighborfoodSimilarity);
}

}


public Double calculateSimrank(int link1, int link2, int counter)
{
	if(counter ==0 || link1 == link2)
	{
		System.out.println("Eimaste ston ypologismo");
		return calculateSimrankScore(link1, link2);
	}
	else if (graph.inLinks(link1).size() == 0 || graph.inLinks(link2).size() == 0)
		return 0.00;
	else
	{
		counter--;
		double value = dampFactor/(graph.inLinks(link1).size() * graph.inLinks(link2).size());
		double sum = 0.00;
		for ( Object inLinksLink1: graph.inLinks(link1).keySet())
		{
		
			for (Object inLinksLink2: graph.inLinks(link2).keySet())
			{
				
				int inlink1 = (Integer) inLinksLink1;
				//System.out.println("Komvos "+inlink1);
				int inlink2 = (Integer) inLinksLink2;
				//System.out.println("Komvos "+inlink2);
				sum = sum + calculatePrank(inlink1,inlink2, counter);
			}
		}
		return sum*value;
	}
}

public Double calculatePrank(int link1, int link2, int counter)
{
	double sum = 0.00;
	// first we calculate for the inLinks
	if(counter ==0 || link1 == link2)
		//If we are at the last iteration or if the two nodes are the same
	{
		//System.out.println("Eimaste ston ypologismo");
		sum= calculateSimrankScore(link1, link2);
	}
	//If there is no inLink
	else if (graph.inLinks(link1).size() == 0 || graph.inLinks(link2).size() == 0)
		sum= 0.00;
	else
	{
		
		//First we reduce the counter
		counter--;
		//Next we calculate the first portion of the equation
		double value = dampFactor/(graph.inLinks(link1).size() * graph.inLinks(link2).size());
		//Next we calculate the total similarity
		for ( Object inLinksLink1: graph.inLinks(link1).keySet())
		{
		
			for (Object inLinksLink2: graph.inLinks(link2).keySet())
			{
				
				int inlink1 = (Integer) inLinksLink1;
				//System.out.println("Komvos "+inlink1);
				int inlink2 = (Integer) inLinksLink2;
				//System.out.println("Komvos "+inlink2);
				sum = sum + calculatePrank(inlink1,inlink2, counter);
			}
		}
		sum= sum*value;
	}
	
	//Step 2: We calculate ranking for Outlinks
	double sumOutLinks = 0.00;
	if(counter ==0 ||link1 == link2)
	{
		//System.out.println("Eimaste ston ypologismo");
		sumOutLinks= calculateSimrankScore(link1, link2);
	}
	else if (graph.outLinks(link1).size() == 0 || graph.outLinks(link2).size() == 0)
		sumOutLinks= 0.00;
	else
	{
		counter--;
		double value = dampFactor/(graph.outLinks(link1).size() * graph.outLinks(link2).size());
		
		for ( Object outLinksLink1: graph.outLinks(link1).keySet())
		{
		
			for (Object outLinksLink2: graph.outLinks(link2).keySet())
			{
				
				int outlinkA = (Integer) outLinksLink1;
				//System.out.println("Komvos "+inlink1);
				int outlinkB = (Integer) outLinksLink2;
				//System.out.println("Komvos "+inlink2);
				sumOutLinks = sumOutLinks + calculatePrank(outlinkA,outlinkB, counter);
			}
		}
		sumOutLinks= sumOutLinks*value;
	}
	return lamda*sum+(1-lamda)*sumOutLinks;
}

public Double calculateOmnirank(int link1, int link2, int counter)
{
	
	
	double sum = 0.00;
	
	// first we calculate for the inLinks
	if(counter ==0 || link1 == link2)
		//If we are at the last iteration or if the two nodes are the same
	{
		//System.out.println("Eimaste ston ypologismo");
		sum= calculateSimrankScore(link1, link2);
	}
	//If there is no inLink
	else if (graph.inLinks(link1).size() == 0 || graph.inLinks(link2).size() == 0)
		sum= 0.00;
	else
	{
		
		//First we reduce the counter
		counter--;
		//Next we calculate the first portion of the equation
		double value = dampFactor/(graph.inLinks(link1).size() * graph.inLinks(link2).size());
		//Next we calculate the total similarity
		for ( Object inLinksLink1: graph.inLinks(link1).keySet())
		{
		
			for (Object inLinksLink2: graph.inLinks(link2).keySet())
			{
				
				int inlink1 = (Integer) inLinksLink1;
				//System.out.println("Komvos "+inlink1);
				int inlink2 = (Integer) inLinksLink2;
				//System.out.println("Komvos "+inlink2);
				sum = sum + calculateOmnirank(inlink1,inlink2, counter);
			}
		}
		sum= sum*value;
	}
	
	//Step 2: We calculate ranking for Outlinks
	double sumOutLinks = 0.00;
	if(counter ==0 ||link1 == link2)
	{
		//System.out.println("Eimaste ston ypologismo");
		sumOutLinks= calculateSimrankScore(link1, link2);
	}
	else if (graph.outLinks(link1).size() == 0 || graph.outLinks(link2).size() == 0)
		sumOutLinks= 0.00;
	else
	{
		counter--;
		double value = dampFactor/(graph.outLinks(link1).size() * graph.outLinks(link2).size());
		
		for ( Object outLinksLink1: graph.outLinks(link1).keySet())
		{
		
			for (Object outLinksLink2: graph.outLinks(link2).keySet())
			{
				
				int outlinkA = (Integer) outLinksLink1;
				//System.out.println("Komvos "+inlink1);
				int outlinkB = (Integer) outLinksLink2;
				//System.out.println("Komvos "+inlink2);
				sumOutLinks = sumOutLinks + calculateOmnirank(outlinkA,outlinkB, counter);
			}
		}
		sumOutLinks= sumOutLinks*value;
	}
	//Step 3: We calculate ranking for parallel links
		double sumParallelInLinks = 0.00;
		if(counter ==0 ||link1 == link2)
		{
			//System.out.println("Eimaste ston ypologismo");
			sumParallelInLinks= calculateSimrankScore(link1, link2);
		}
		else if (parallelInLinks.get(link1).size() ==0 || parallelInLinks.get(link2).size() ==0)
			sumParallelInLinks= 0.00;
		else
		{
			counter--;
			double value = dampFactor/(parallelInLinks.get(link1).size() * parallelInLinks.get(link2).size());
			
			for ( Object inParallelLinksLink1: parallelInLinks.get(link1).keySet())
			{
			
				for (Object inParallelLinksLink2: parallelInLinks.get(link2).keySet())
				{
					
					int outlinkA = (Integer) inParallelLinksLink1;
					//System.out.println("Komvos "+inlink1);
					int outlinkB = (Integer) inParallelLinksLink2;
					//System.out.println("Komvos "+inlink2);
					sumParallelInLinks = sumParallelInLinks + calculateOmnirank(outlinkA,outlinkB, counter);
				}
			}
			sumParallelInLinks= sumParallelInLinks*value;
		}
		
		//Step 4: We calculate ranking for parallel out links
				double sumParallelOutLinks = 0.00;
				if(counter ==0 ||link1 == link2)
				{
					//System.out.println("Eimaste ston ypologismo");
					sumParallelOutLinks= calculateSimrankScore(link1, link2);
				}
				else if (parallelOutLinks.get(link1).size() ==0 || parallelOutLinks.get(link2).size() ==0)
					sumParallelOutLinks= 0.00;
				else
				{
					counter--;
					double value = dampFactor/(parallelOutLinks.get(link1).size() * parallelOutLinks.get(link2).size());
					
					for ( Object outParallelLinksLink1: parallelOutLinks.get(link1).keySet())
					{
					
						for (Object outParallelLinksLink2: parallelOutLinks.get(link2).keySet())
						{
							
							int outlinkA = (Integer) outParallelLinksLink1;
							//System.out.println("Komvos "+inlink1);
							int outlinkB = (Integer) outParallelLinksLink2;
							//System.out.println("Komvos "+inlink2);
							sumParallelOutLinks = sumParallelOutLinks + calculateOmnirank(outlinkA,outlinkB, counter);
						}
					}
					sumParallelOutLinks= sumParallelOutLinks*value;
				}
				
				return (alpha*sum) + (beta*sumOutLinks) + (gama*sumParallelInLinks)+ (delta*sumParallelOutLinks);
				//return (alpha*sum) + (beta*(1-alpha)*sumOutLinks) + (gama*(1-alpha)*(1-beta)*sumParallelInLinks) + ((1-alpha)*(1-beta)*(1-gama) * sumParallelOutLinks);
}

private Double calculateSimrankScore(int link1, int link2) {
	if(link1 == link2)
		return 1.00;
	else
		return 0.00;
}

private void discoverParallelLinks (WebGraph wg, WebGraph wgWithParallelLinks, Map<Integer, Map> parallelInLinks2, Map<Integer, Map> parallelOutLinks2 )
{
	for (int i=1; i<=wgWithParallelLinks.numNodes();i++)
	{
		Map<Integer,Double> inParallelLinks = new HashMap<Integer, Double>();
		Map<Integer,Double> outParallelLinks = new HashMap<Integer, Double>();
		for(Object outLinksLink1: wgWithParallelLinks.outLinks(i).keySet())
		{
			if(wgWithParallelLinks.IdentifyerToURL(Integer.valueOf(outLinksLink1.toString())).contains(wgWithParallelLinks.IdentifyerToURL(i).substring(0, 1)))
			{
				System.out.println("vrhkame parallel link: " + wgWithParallelLinks.IdentifyerToURL(Integer.valueOf(outLinksLink1.toString())) + " me "+wgWithParallelLinks.IdentifyerToURL(i));
				inParallelLinks.put((Integer) outLinksLink1, 1.00);
			outParallelLinks.put(i, 1.00);
		//	wg.addLink(wg.IdentifyerToURL(i), wg.IdentifyerToURL(Integer.valueOf(outLinksLink1.toString())), 0.00);
			
		}}
		parallelInLinks2.put(i, inParallelLinks);
		parallelOutLinks2.put(i, outParallelLinks);
		
		
	}
	
}
}