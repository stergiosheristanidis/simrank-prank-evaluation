# Simrank - P-Rank Evaluation #

By using this project, you will be able to evaluate three different link prediction algorithms, Simrank, P-Rank and OmniRank on two different datasets.

### What is this repository for? ###

This is the 1.0 version of the evaluation of three algorithms. For more details, you can refer to http://ikee.lib.auth.gr/record/282900/files/GRI-2016-16525.pdf, which is the link of my thesis and explains better the way the three different algorithms work.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
